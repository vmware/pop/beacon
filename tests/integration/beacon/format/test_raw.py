def test_apply(hub):
    event = object()
    result = hub.beacon.format.raw.apply(event, __name__)

    assert result is event
