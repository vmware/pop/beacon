def test_apply(hub, event):
    result = hub.beacon.format.json.apply(event, __name__)

    assert result.pop("timestamp")
    assert result["ref"] == __name__
    assert event == result["data"]
