import asyncio

import pytest


@pytest.mark.asyncio
async def test_listen(hub, beacon, event):
    """
    Test the full path of an event, from ingress to ingress
    """
    await hub.beacon.local.queue()
    await hub.ingress.init.queue()

    # Put an event on the queue
    await hub.beacon.local.QUEUE.put(event)

    await asyncio.sleep(0, loop=hub.pop.Loop)

    # Verify that the event made it all the way through to being published
    result = await hub.ingress.QUEUE.get()

    assert result["timestamp"]
    assert result["ref"] == "beacon.local"
    assert event == result["data"]
