import asyncio
import logging
import uuid
from typing import Any
from typing import Dict
from typing import List
from typing import Tuple
from unittest import mock

import pytest


log = logging.getLogger(__name__)

DEFAULT_PROFILE = "beacon_test_profile"


@pytest.fixture(scope="function")
@pytest.mark.asyncio
async def hub(hub, event_loop):
    hub.pop.loop.CURRENT_LOOP = event_loop
    hub.pop.Loop = event_loop
    hub.pop.loop.create = mock.MagicMock()
    log.debug("Initializing hub with beacons")
    hub.pop.sub.add(dyne_name="beacon")

    with mock.patch("sys.argv", ["beacon"]):
        hub.pop.config.load(["beacon", "acct", "evbus", "rend"], cli="beacon")

    log.debug("Initialized hub with beacons")
    yield hub


async def start_beacon(
    hub, profiles: Dict[str, Dict[str, Any]] = None
) -> Tuple[List[asyncio.Task], Dict[str, Dict[str, Any]]]:
    log.debug("Setting up beacon fixture")
    sub_profiles = await hub.acct.init.process(subs=["beacon"], profiles=profiles)

    tasks = [
        hub.pop.loop.CURRENT_LOOP.create_task(t)
        for t in (
            hub.evbus.init.start(sub_profiles={}),
            hub.beacon.init.start(format_plugin="json", sub_profiles=sub_profiles),
        )
    ]

    log.debug("Waiting for plugins to initialize")
    await hub.beacon.init.join()

    log.debug("beacon fixture is ready")

    return tasks, sub_profiles


@pytest.mark.asyncio
@pytest.fixture(scope="function")
async def beacon(hub):
    """
    A bare minimum beacon with only local queues and no outside connections
    """
    tasks, _ = await start_beacon(hub, {})

    yield

    await stop_beacon(hub, tasks)


async def stop_beacon(hub, tasks: List[asyncio.Task]):
    hub.log.debug("Stopping beacon-based fixture")
    # Cleanup acct

    # Stop beacons
    await hub.beacon.init.stop()

    for task in asyncio.as_completed(tasks, loop=hub.pop.Loop):
        await task


@pytest.fixture(scope="function")
def event():
    e = f"bacon-event-{uuid.uuid4()}"
    log.debug(f"created event: {e}")
    return e


@pytest.mark.asyncio
@pytest.fixture(scope="function")
async def rabbitmq_beacon(hub):
    """
    Start beacons with a rabbitmq connection
    """
    channel_name = f"bacon-test-{uuid.uuid4()}"

    # TODO get these by setting hub.OPT.acct.acct_file and hub.OPT.acct.acct_key from the cli for CI
    # Credentials for connecting to a local rabbitmq server
    profiles = {
        "pika": {
            DEFAULT_PROFILE: {
                "host": "localhost",
                "port": 5672,
                "beacon_channels": [channel_name],
            }
        }
    }

    tasks, sub_profiles = await start_beacon(hub, profiles=profiles)

    ctx = sub_profiles.beacon.pika[DEFAULT_PROFILE]

    if not ctx.connected:
        pytest.skip("No local rabbitmq server")

    yield ctx, channel_name

    # Cleanup
    channel = await ctx.connection.channel()
    await channel.queue_delete(channel_name)

    await stop_beacon(hub, tasks)
